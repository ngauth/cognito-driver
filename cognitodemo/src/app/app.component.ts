import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { AppConfig } from '@ngcore/core';
import { CustomConfig, ConfigFactory } from '@ngcore/core';
import { LoggedInStatus } from '@ngauth/core';
import { AuthConfigCallback, ICognitoAuthConfig, DefaultCognitoAuthConf } from '@ngauth/core';
import { AuthConfigManager } from '@ngauth/services';
import {
  AuthReadyComponent,
  UserStateComponent,
  UserLoginComponent,
  UserLogoutComponent,
  NewRegistrationComponent,
  ConfirmRegistrationComponent,
  ResendCodeComponent,
  ChangePasswordComponent,
  ResetPasswordStep1Component, ResetPasswordStep2Component,
} from '@ngauth/forms';

import {
  UserLoginModalComponent,
  UserLogoutModalComponent,
  NewRegistrationModalComponent,
  ResendCodeModalComponent,
  ConfirmRegistrationModalComponent,
  ChangePasswordModalComponent,
  ResetPasswordModalComponent
} from '@ngauth/modals';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AuthConfigCallback {
  title = 'NGAuth/Cognito Demo';

  @ViewChild("ngAuthAuthReady") ngAuthAuthReady: AuthReadyComponent;
  @ViewChild("ngAuthUserState") ngAuthUserState: UserStateComponent;
  @ViewChild("ngAuthUserLogout") ngAuthUserLogout: UserLogoutComponent;


  // testing
  region: string = "";

  authConfLoaded: boolean = false;

  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfig,
    private configFactory: ConfigFactory,
    private authConfigManager: AuthConfigManager
  ) {
    this.authConfigManager.addCallback(this);
  }

  authConfigLoaded(): void {
    this.authConfLoaded = true;
    if (this.ngAuthAuthReady) {
      this.ngAuthAuthReady.refreshUI();
    }
    if (this.ngAuthUserState) {
      // this.ngAuthUserState.refreshUI();
      this.ngAuthUserState.checkLoginStatus();
    }
  }

  // Testing app config.
  ngOnInit(): void {

    // testing
    let val0 = this.appConfig.getString("key0");
    if(isDL()) dl.log("val0 = " + val0);
    // testing

    // testing
    let configFile = "custom-config";
    this.configFactory.buildConfig(configFile).subscribe((data) => {
      if(dl.isLoggable()) dl.log("data = " + data);
      let customConfig = data;
      let val1 = customConfig.get("key1");
      if(dl.isLoggable()) dl.log("val1 = " + val1);
    });
    // testing

    // // testing
    // // let region = this.authConfigManager.region;
    // // if(isDL()) dl.log("region = " + region);
    // this.authConfigManager.loadConfig().subscribe((authConfig) => {
    //   let region = authConfig.region;
    //   if(isDL()) dl.log("region = " + region);

    //   this.authConfLoaded = this.authConfigManager.isLoaded;
    //   if(isDL()) dl.log("authConfLoaded = " + this.authConfLoaded);

    //   // Auth related components can be used only after authConfLoaded == true.
    //   // ....
    // });
    // // testing


    // // testing
    // this.authConfigManager.triggerLoading();
    // // testing

  }


  onUserStateChecked(loggedInStatus: LoggedInStatus) {
    // if(isDL()) dl.log("loggedInStatus = " + loggedInStatus.toString());
    if(isDL()) dl.log(`loggedInStatus =  ${loggedInStatus.toString()}`);

  }


  openUserLoginDialog() {
    let dialogRef = this.dialog.open(UserLoginModalComponent, {
      width: '280px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(loggedIn => {
      if(dl.isLoggable()) dl.log('The dialog was closed. loggedIn = ' + loggedIn);

      if (loggedIn === true || loggedIn === false) {
        this.ngAuthUserState.updateLoginStatus(loggedIn);
      }
    });
  }

  handleUserLogout() {
    if(isDL()) dl.log('handleUserLogout button clicked.');

    this.ngAuthUserLogout.logout();
  }

  onUserLoggedOut(loggedInStatus: LoggedInStatus) {
    if(isDL()) dl.log(`loggedInStatus =  ${loggedInStatus.toString()}`);

    if (loggedInStatus.loggedIn === true || loggedInStatus.loggedIn === false) {
      this.ngAuthUserState.updateLoginStatus(loggedInStatus.loggedIn);
    }
  }


  handleNewRegistration() {
    if(isDL()) dl.log('handleNewRegistration button clicked.');

    let dialogRef = this.dialog.open(NewRegistrationModalComponent, {
      width: '320px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(dl.isLoggable()) dl.log('The dialog was closed. result = ' + result);
    });
  }

  handleResendCode() {
    if(isDL()) dl.log('handleResendCode button clicked.');

    let dialogRef = this.dialog.open(ResendCodeModalComponent, {
      width: '320px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(dl.isLoggable()) dl.log('The dialog was closed. result = ' + result);
    });
  }

  handleConfirmRegistration() {
    if(isDL()) dl.log('handleConfirmRegistration button clicked.');

    let dialogRef = this.dialog.open(ConfirmRegistrationModalComponent, {
      width: '280px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(dl.isLoggable()) dl.log('The dialog was closed. result = ' + result);
    });
  }

  handleChangePassword() {
    if(isDL()) dl.log('handleChangePassword button clicked.');

    let dialogRef = this.dialog.open(ChangePasswordModalComponent, {
      width: '280px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(dl.isLoggable()) dl.log('The dialog was closed. result = ' + result);
    });
  }

  handleResetPassword() {
    if(isDL()) dl.log('handleResetPassword button clicked.');

    let dialogRef = this.dialog.open(ResetPasswordModalComponent, {
      width: '320px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(dl.isLoggable()) dl.log('The dialog was closed. result = ' + result);
    });
  }


}
