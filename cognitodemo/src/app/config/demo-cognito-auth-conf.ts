import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { CustomConfig, ConfigFactory } from '@ngcore/core';
import { ICognitoAuthConfig, CognitoAuthConfig, DefaultCognitoAuthConf } from '@ngauth/core';


@Injectable()
export class DemoCognitoAuthConf
  extends DefaultCognitoAuthConf
  // implements ICognitoAuthConfig 
{
  // temporary
  private static CONFIG_FILE = "auth-config";

  // private authConfig: (CognitoAuthConfig | null) = null;
  // // private _isLoaded = false;

  private _config: (CustomConfig | null) = null;
  constructor(private configFactory: ConfigFactory) {
    super();
    // this.configFactory.buildConfig(DemoCognitoAuthConfig.CONFIG_FILE)
    // .subscribe((config) => {
    //   this._config = config;
    //   this._isLoaded = true;
    // })
  }


  loadConfig(): Observable<ICognitoAuthConfig> {
    return Observable.create((obs) => {
      if(this.authConfig == null) {
        this.configFactory.buildConfig(DemoCognitoAuthConf.CONFIG_FILE)
          .subscribe((config) => {
            this._config = config;

            let ac = new CognitoAuthConfig();
            ac.region = this._config.getString("region");
            ac.identityPoolId = this._config.getString("identityPoolId");
            ac.userPoolId = this._config.getString("userPoolId");
            ac.userPoolClientId = this._config.getString("userPoolClientId");
            ac.userIdTableName = this._config.getString("userIdTableName");
            // ...

            // this._isLoaded = true;
            this.authConfig = ac;
            obs.next(ac);
          });
        } else {
          obs.next(this.authConfig);
        }
    }).share();  // ???
  }

  // get isLoaded(): boolean {
  //   // return this._isLoaded;
  //   return !!this.authConfig;
  // }


  // Use of these individual field getters are deprecated.
  // Use loadConfig() instead.
  // To be deleted.

  get region(): (string | null) {
    let region = (this._config) ? this._config.get("region") as string : null;
    if(isDL()) dl.log(">>>> region = " + region);
    return region;
  }

  get identityPoolId(): (string | null) {
    return (this._config) ? this._config.get("identityPoolId") as string : null;
  }

  get userPoolId(): (string | null) {
    return (this._config) ? this._config.get("userPoolId") as string : null;
  }

  get userPoolClientId(): (string | null) {
    return (this._config) ? this._config.get("userPoolClientId") as string : null;
  }

  get userIdTableName(): (string | null) {
    return (this._config) ? this._config.get("userIdTableName") as string : null;
  }

}


