import { Injectable } from '@angular/core';

import { IAuthRouteInfo, AuthRouteInfo, DefaultAuthRoutes } from '@ngauth/core';


@Injectable()
export class DemoAuthRoutes extends DefaultAuthRoutes {

  // Testing ...
  // private static _routeInfo: AuthRouteInfo = new AuthRouteInfo(
  //   '/home/login',
  //   '/home/logout'
  // );
  private static getRouteInfo(): AuthRouteInfo {

    let routeInfo = new AuthRouteInfo(
      '/home',
      '/home/login',
      '/home/logout',
      '/home/register',
      '/home/confirm',
      '/home/resend',
      '/home/change',
      '/home/reset'
    );
    return routeInfo;
  }

  constructor() {
    // super(DemoAuthRoutes.getRouteInfo());
    super();
    this.authRouteInfo = DemoAuthRoutes.getRouteInfo();
  }

}
