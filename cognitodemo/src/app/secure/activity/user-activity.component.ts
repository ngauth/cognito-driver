import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;

import { DefaultAuthRoutes } from '../../route/default-auth-routes';
import { LoggedInCallback } from '../../core/auth-callbacks';
import { LogItem } from '../../common/log-item';
import { DdbServiceUtil } from '../../services/cognito/ddb-service-util';
import { UserLoginHelper } from '../../services/cognito/user-login-helper';


@Component({
  selector: 'ngauth-modal-user-activity',
  templateUrl: './user-activity.component.html'
})
export class UserActivityComponent implements OnInit, LoggedInCallback {

  public logdata: Array<LogItem> = [];

  constructor(
    private router: Router,
    private authRoutes: DefaultAuthRoutes,
    private ddbUtil: DdbServiceUtil,
    private userService: UserLoginHelper
  ) {
  }

  ngOnInit(): void {
    this.userService.isAuthenticated(this);
    if(isDL()) dl.log("in UserActivityComponent");
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (!isLoggedIn) {
      // tbd:
      this.router.navigate([this.authRoutes.authRouteInfo.login]);
      // ...
    } else {
      if(isDL()) dl.log("scanning DDB");
      this.ddbUtil.getLogEntries(this.logdata);
    }
  }

}
